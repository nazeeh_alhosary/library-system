<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
//When open the login page initialize the session 'startSession' to true.
boolean checkSession = true; 
session.setAttribute("startSession",checkSession);
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
	<title>Nazeeh | Login </title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="web_app/public/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>
<body>
<div class="container">


<div class="jumbotron text-center mt-5">
	    <img src="web_app/public/img/enter.svg" width="100" height="100" class="d-inline-block align-top" alt="" >

  <h1 class="display-3">Welcome!</h1>
  <p class="lead"><strong>Please log in</strong> for further instructions on how to complete your account setup.</p>
  <hr>
  <p>
    Having trouble? <a href="nazeeh_alhosart">Contact us</a>
  </p>
  <p class="lead">
  <a href="<%= request.getContextPath()%>" role="button" class="btn btn-success btn-lg mr-3"> <span class="fas fa-sign-in-alt" ></span> Login</a>

   
  </p>
</div>
</div>


<script type="text/javascript">

/*
	When opening this page, it will clear the localStorage because The DataTables storage its values in the localStorage of the browser.
*/
$( document ).ready(function() {
	localStorage.clear();
});


</script>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

</body>
</html>
