<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Library Items";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<%  

//To be able to set the session 'LibraryItemAdded' every time we add a new Library Item. When opening the Library Items page, it will kill the session 'LibraryItemAdded'

session.removeAttribute("LibraryItemAdded");
%>

<div class="card text-center mt-4 mb-4">
  <div class="card-header  ">
    <!-- Add Text if needed -->
  </div>
  <div class="card-body">
    <h5 class="card-title">Add Library Item</h5>
    <p class="card-text">If you want to add a new library item, you can click on the button below.</p>
    <a href="add_library_item" class="btn btn-success  btn-lg"> <span class="fas fa-folder-plus"></span>&ensp;Add Library Item</a>
  </div>
  <div class="card-footer text-muted">
     <!-- Add Text if needed -->
  </div>
</div>

<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>
<%@page errorPage ="library_items_error" %>

<%@page import="java.sql.*" %>
<%@page import="java.util.List" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>




<table class="table table-hover table-bordered libraryItemTable" id="libraryItemTable">
<thead>
    <tr class="table-light text-center">
        <th scope="col" >Type</th>
		<th scope="col">Category</th>
		<th scope="col" >Title</th>
		<th scope="col" >Author</th>
		<th scope="col" >#Pages</th>
		<th scope="col" >#Minutes</th>
		<th scope="col" >IsBorrowable?</th>
		<th scope="col" >Borrower</th>
		<th scope="col" >BorrowDate</th>
		<th scope="col" >Actions </th>
    </tr>
  </thead>

  <% List<LibraryItem> itemList = LibraryItemModel.getAllRecords(); %>
  


	<tbody>
	<% for(LibraryItem libraryItem:itemList){ %>
		<tr class=" text-center">
			<td scope="row"> <%= libraryItem.getType()%> </td>
			<td scope="row"> <%= libraryItem.getCategoryName()%> </td>
			<td scope="row"> <%= libraryItem.getTitle()%> </td>
			<td scope="row">
				<% if(libraryItem.getAuthor() == null || libraryItem.getAuthor().isEmpty()){ //.equals("") %>
					&horbar;
				<% }else{ %>
					<%= libraryItem.getAuthor()%>
				<%} %>			
			 </td>
			<td scope="row">
				<% if( libraryItem.getPages() == 0){ %>
					&horbar;
				<% }else{ %>
					<%= libraryItem.getPages() %>	
				<%} %>
			 </td>
			<td scope="row"> 
				<% if( libraryItem.getRunTimeMinutes() == 0){ %>
					&horbar;
				<% }else{ %>
					<%= libraryItem.getRunTimeMinutes() %>	
				<%} %>
			
			 </td>
			
			<td scope="row">
				<% if(libraryItem.isIsBorrowable()){ //&#10003; %>
					&#10004; 
				<% }else{ %>
					&#10060;
				<%} %>
 			</td>
			<td scope="row">
				<% if(libraryItem.getBorrower() == null || libraryItem.getBorrower().isEmpty()){ //.equals("") %>
					&horbar;
				<% }else{ %>
					<%= libraryItem.getBorrower()%>
				<%} %>
			</td>
			<td scope="row">
				<% if(libraryItem.getBorrowDate() == null || libraryItem.getBorrowDate().isEmpty()){ //.equals("") %>
					&horbar;
				<% }else{ %>
					<%= libraryItem.getBorrowDate()%>
				<%} %>
				
			</td>
			
			<td>
			
			 <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Delete <%= libraryItem.getTitle() %>"> <a href="delete_library_item?id=<%= libraryItem.getId() %>" class="text-danger"><span class="fas fa-trash-alt" ></span> </a> </span>
			  &ensp; 
			  <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Edit <%= libraryItem.getTitle() %>"><a href="edit_library_item?id=<%= libraryItem.getId() %>"  class="text-info"> <span class="fas fa-edit"></span></a> </span> 
			   &ensp; 

				  <% if(libraryItem.isIsBorrowable()){ //&#10003; %>
					<span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="CheckOut <%= libraryItem.getTitle() %>"> 
						<a href="checkout?id=<%= libraryItem.getId() %>" role="button" class="badge badge-pill badge-warning">
						 <span class="fas fa-sign-out-alt">
						 </span> &ensp; CheckOut
						 </a>
					</span>
			
				<% }else if(!libraryItem.getType().equalsIgnoreCase("Reference Book")){ %>
					<span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="CheckIn <%= libraryItem.getTitle() %>"> 
						<a href="checkin_controller?id=<%= libraryItem.getId() %>"  role="button" class="badge badge-pill badge-primary">
						 <span class="fas fa-sign-in-alt">
						  </span> &ensp; CheckIn
						 </a>
					</span>

				<%} else{%>
					<span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="This is a book you read at the library but can't be borrowed home"> 
						<a href="#"  role="button" class="badge badge-pill badge-danger">
						 <span class="fas fa-book">
						  </span> &ensp; Reference Book
						 </a>
					</span>

				<%} %>
			</td>  
		</tr>

<% } %>
	</tbody>
</table>

<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

 <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
 	<script type="text/javascript">
 	
 	 /*
		When opening the page, we will use the plug-in DataTable (see more: https://datatables.net/) to sort the table/ search for something faster.
		DataTables used to reduce the number of queries when sorting by another column.
		To be able to save the status of the table (the status here is saving which column selected and don't change it on reloading the page or when moving between pages),
		I used the boolean session 'startSession' that should be initialized when opening the home page or login (start the application) and it will be killed when logging out
		(closing the application). If the session for some reasons did not initialize the state of the table will be set to false (Which means re-set everything even on-loading).
		The session value will be collected from the method getSessionStatus()
	*/	
 	$(document).ready(function() {
 	   $('#libraryItemTable').DataTable({
 		  "aLengthMenu": [[15, 30, 50, -1], [15, 30, 50, "All"]],
 		  'pageLength': 15,
 		  'language': {
 		    search: '',
 		    searchPlaceholder: "Search..."
 		  },
 		  'searching': true,
 		  'bLengthChange': true,
 		  'bPaginate': true,
 		  'order': [[1, 'asc']],
 		  'paging': true,
 		   stateSave: getSessionStatus(),
 		  'bInfo': false,
 		  'columnDefs': [{
 	 		    'orderable': false,
 	 		    'targets': [9]
 		  }],		  
 			});
 	} );
 	
 	 /*
 		Check if the session 'startSession' is set, returns true otherwise returns false.
	 */
 	function getSessionStatus(){
		<%
			String sessionValue = null;
			if(session.getAttribute("startSession")!=null){
				sessionValue = session.getAttribute("startSession").toString();
			}
		%> 
		var theSession = <%=sessionValue %>; 
		return theSession ? true : false;
	}
 	

 	</script>

 
</body>
</html>