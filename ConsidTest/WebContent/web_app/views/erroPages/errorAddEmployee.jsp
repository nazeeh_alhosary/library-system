<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Error!|Employee";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>



<%-- <%@ includefile="../../inc/header.html" %> --%>
<jsp:include page="/web_app/inc/header.jsp"></jsp:include>

<%@page isErrorPage ="true" %>



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
$( document ).ready(function() {
	  <% String getMessage= exception.getMessage(); %>
	   var message="<%=getMessage%>"; 
	   console.log(message);
	   swal({
		   title: "Oops!",
		   text: "Error! "+message + ".\n Please try with valid input.",
		   icon: "error",  
		   dangerMode: true, 
	   });
});
</script> 
<jsp:include page="/web_app/forms/addEmployeeForm.jsp"></jsp:include>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>


</body>
</html>