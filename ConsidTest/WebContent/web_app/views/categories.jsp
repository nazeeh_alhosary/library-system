<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Categories";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>



<jsp:include page="/web_app/inc/header.jsp"></jsp:include>

<% 
//To be able to set the session 'categoryAdded' every time we add a new category. When opening the category page, it will kill the session 'categoryAdded'
session.removeAttribute("categoryAdded");
%>
<div class="card text-center mt-4 mb-4">
  <div class="card-header  ">
    <!-- Add Text if needed -->
  </div>
  <div class="card-body">
    <h5 class="card-title">Add Category</h5>
    <p class="card-text">If you want to add a new category, you can click on the button below.</p>
    <a href="add_category" class="btn btn-success btn-lg"> <span class="fas fa-plus"></span>&ensp;Add Category</a>
  </div>
  <div class="card-footer text-muted">
     <!-- Add Text if needed -->
  </div>
</div>

<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>
<%@page errorPage ="categories_error" %>

<%@page import="java.sql.*" %>
<%@page import="java.util.List" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>




<table class="table table-hover categoryTable" id="categoryTable">
<thead>
    <tr class="table-light">
      <th scope="col" >Name</th>

      <th scope="col">Actions</th>
    </tr>
  </thead>
  <% List<Category> list = CategoryModel.getAllRecords(); %>
	
	<tbody>
		<% for(Category category:list){ %>
			<tr>
			    <td scope="row"> <%= category.getCategoryName() %> </td>
				<td> <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Delete <%= category.getCategoryName() %>"> <a href="delete_category?id=<%= category.getId() %>" class="text-danger"><span class="fas fa-trash-alt" ></span> </a> </span>
				  &ensp; 
				  <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Edit <%= category.getCategoryName() %>"><a href="edit_category?id=<%= category.getId() %>"  class="text-info"> <span class="fas fa-edit"></span></a> </span> </td>
			</tr>
		<% } %>
</tbody>
</table>

<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

 <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
 	<script type="text/javascript">
 /*
	When opening the page, we will use the plug-in DataTable (see more: https://datatables.net/) to sort the table/ search for something faster.
	DataTables used to reduce the number of queries when sorting by another column.
	To be able to save the status of the table (the status here is saving which column selected and don't change it on reloading the page or when moving between pages),
	I used the boolean session 'startSession' that should be initialized when opening the home page or login (start the application) and it will be killed when logging out
	(closing the application). If the session for some reasons did not initialize the state of the table will be set to false (Which means re-set everything even on-loading).
	The session value will be collected from the method getSessionStatus()
*/
 	$(document).ready(function() {
 	   $('#categoryTable').DataTable({
 		  "aLengthMenu": [[15, 30, 50, -1], [15, 30, 50, "All"]],
 		  'pageLength': 15,
 		  'language': {
 		    search: '',
 		    searchPlaceholder: "Search..."
 		  },
 		 'searching': true,
		  'bLengthChange': true,
		  'bPaginate': true,
		  'paging': true,
		  stateSave: getSessionStatus(),
 		  'bInfo': false,
 		 'order': [[0, 'asc']]
 	  });
 	   
 	} );
 	
 /*
 	Check if the session 'startSession' is set, returns true otherwise returns false.
 */
 	function getSessionStatus(){
		<%
			String sessionValue = null;
			if(session.getAttribute("startSession")!=null){
				sessionValue = session.getAttribute("startSession").toString();
			}
		%> 
		var theSession = <%=sessionValue %>; 
		return theSession ? true : false;
	}
 	</script>

 
</body>
</html>
