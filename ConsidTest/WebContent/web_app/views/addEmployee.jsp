<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Add Employee";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>

<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>

<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>


<%@page import="java.util.*" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="libraryItem" class="AppObjects.LibraryItem"></jsp:useBean>
<jsp:setProperty property="*" name="libraryItem"/>

<jsp:useBean id="employee" class="AppObjects.Employee"></jsp:useBean>
<jsp:setProperty property="*" name="employee"/>


<%@page errorPage ="employee_error" %>

<%
/**
	To avoid having two files for adding a new Employee We will check first if the request is POST request or not
	to be able to insert a new item in the database. 
	(I colud have two files such as addEmployee.jsp abd addEmployeeController.jsp, but this is faster).
	If the request is POST, we will insert a new employee. if there is an error when adding a new employee it will 
	show an error message that comes from the "employee_error" error page. otherwise it will set the new session 
	called 'EmployeeAdded' to use its value in the alert method below. 
	Finally, redirect the page to itself (Which is a GET request) to show the alert message and let the user choose 
	if he/she wants to add more or go to the list.
*/
if ("POST".equalsIgnoreCase(request.getMethod())) {
	//Get the boolean Variable from the post and assign them to the employee
	//isCEO
	if(request.getParameter("isCEO") !=null){
		employee.setIsCEO(true);
	}else{
		employee.setIsCEO(false);
	}
	//isManager
	if(request.getParameter("isManager") !=null){
		employee.setIsManager(true);
	}else{
		employee.setIsManager(false);
	}
	int restul = EmployeeModel.insert(employee);
 
	if(restul>0){
		session.setAttribute("EmployeeAdded","New Employee has been added!");
		response.sendRedirect("add_employee");
	}
	
}
%>


<jsp:include page="/web_app/forms/addEmployeeForm.jsp"></jsp:include>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
/*
	When opening the page, the the session 'EmployeeAdded' set as described above, it will show the user the sweetalert. 
	If the user clicke 'Go To Employee List' it will call the method redirectTo() to redirect to the Employee page.
	If the user clicks 'Add more' it will close the alert.
*/
$( document ).ready(function() {
	  <% 
	  String getSession = (String) session.getAttribute("EmployeeAdded");
	  %> 
	   var sessionValue="<%=getSession %>"; 

	   if(sessionValue !== 'null'){
		   console.log(sessionValue);
		   swal({
			   title: "Well Done!",
			   text: "New Employee has been added successfully! ",
			   icon: "success",
			   buttons: ["Add more", "Go To Employee List"],
			   //dangerMode: true,
			 })
			 .then((willDelete) => {
			   if (willDelete) {	
				   redirectTo('/employees');
			   }
			 });
	   }
});

/* 
	The method redirectTo(urlPattern):
	Since all used URLs declared in the web.xml file in the same way (projectName/URL), we can redirect to the wanted page without any problem		
	To avoid hardcoded the project name, we getPath from the browser URL which is the project name, then redirect to the urlPattern page
*/

function redirectTo(urlPattern){
	<% String getPath = request.getContextPath(); %> 
	var path = "<%=getPath %>"; 
	window.location.href = path + urlPattern;
}
</script>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>
 
</body>
</html>