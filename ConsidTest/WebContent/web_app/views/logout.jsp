<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
	<title>Nazeeh | Logout </title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="web_app/public/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>
<body>
<%
session.invalidate();
%>
<div class="container">


<div class="jumbotron text-center mt-5">
	    <img src="web_app/public/img/checked1.svg" width="100" height="100" class="d-inline-block align-top" alt="" >

  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>For using this system</strong> for further instructions on how to complete your account setup.</p>
  <hr>
  <p>
    Having trouble? <a href="nazeeh_alhosart">Contact us</a>
  </p>
  <p class="lead">
  <button class="btn btn-primary btn-sm" id="BackTologin" >Back to Login</button>
   
  </p>
</div>
</div>


<script type="text/javascript">
/*
	When opening this page, it will clear the localStorage because The DataTables storage its values in the localStorage of the browser.
*/
$( document ).ready(function() {
	localStorage.clear();
});
/*
	When the user click on 'Back to Login' button it will redercit to the login page.
*/
$('#BackTologin').on('click',function(){
	redirectTo('/login');
});

/* 
	The method redirectTo(urlPattern):
	Since all used URLs declared in the web.xml file in the same way (projectName/URL), we can redirect to the wanted page without any problem		
	To avoid hardcoded the project name, we getPath from the browser URL which is the project name, then redirect to the urlPattern page
*/
function redirectTo(urlPattern){
	<% String getPath = request.getContextPath(); %> 
	var path = "<%=getPath %>"; 
	window.location.href = path + urlPattern;
}
</script>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

</body>
</html>
