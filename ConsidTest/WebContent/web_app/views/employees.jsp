<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Employees";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<% 
//To be able to set the session 'EmployeeAdded' every time we add a new Employee. When opening the Employee page, it will kill the session 'EmployeeAdded'
session.removeAttribute("EmployeeAdded");
%>
<div class="card text-center mt-4 mb-4">
  <div class="card-header  ">
   
  </div>
  <div class="card-body">
    <h5 class="card-title">Add Employee</h5>
    <p class="card-text">If you want to add a new employee, you can click on the button below.</p>
    <a href="add_employee" class="btn btn-success  btn-lg"> <span class="fas fa-user-plus"></span>&ensp;Add Employee</a>
  </div>
  <div class="card-footer text-muted">
  
  </div>
</div>

<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>
<%@page errorPage ="employees_error" %>
<%@page import="java.sql.*" %>
<%@page import="java.util.List" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>




<table class="table table-hover table-bordered employeesTable" id="employeesTable">
<thead>
    <tr class="table-light text-center">
    <th scope="col" >#ID</th>
        <th scope="col" >First Name</th>
		<th scope="col">Last Name</th>
		<th scope="col" >Salary</th>
		<th scope="col">IsCEO?</th>
		<th scope="col">IsManager?</th>
		<th scope="col" >Role</th>
		<th scope="col" >Manager</th>		
		<th scope="col" style="display:none;">Manager id</th>
		<th scope="col" >Actions </th>
    </tr>
  </thead>

  <%
  	List<Employee> employees = EmployeeModel.getAllRecordsWithManagerNames();
  %>


	<tbody>
	<% for(Employee employee:employees){ %>
		<tr class=" text-center">
			<td scope="row"><%=  employee.getId()%>  </td>
			<td scope="row"><%=  employee.getFirstName()%>  </td>
			<td scope="row"> <%= employee.getLastName()%> </td>
			<td scope="row"> <%= employee.getSalary()%> </td>
			<td scope="row">
				<% if(employee.isIsCEO()){ //.equals("") %>
					&#10004; 
				<% }else{ %>
					&#10060;
				<%} %>			
			 </td>
			<td scope="row">
				<% if( employee.isIsManager()){ %>
					&#10004;
				<% }else{ %>
					&#10060;
				<%} %>
			 </td>
			 
			  <td scope="row"> 
			  <%= employee.getRole() %>
			 </td>
			 
			<td scope="row"> 
				<% if(employee.getManagerId()>0){ %>
					<%= employee.getManagerFullName() %>
				<% }else{ %>
					&horbar;	
				<%} %>
			
			 </td>

			<td scope="row" style="display:none;"> 
				<% if(employee.getManagerId()>0){ %>
					 <%= employee.getManagerId() %> 
				<% }else{ %>
					&horbar;	
				<%} %>
			
			 </td>			 
			
			<td>
			
			 <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Delete <%= employee.getFirstName() %> <%= employee.getLastName()%>"> <a href="delete_employee?id=<%= employee.getId() %>" class="text-danger"><span class="fas fa-trash-alt" ></span> </a> </span>
			  &ensp; 
			  <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="top" title="Edit <%= employee.getFirstName() %> <%= employee.getLastName()%>""><a href="edit_employee?id=<%= employee.getId() %>"  class="text-info"> <span class="fas fa-edit"></span></a> </span> 
			</td>  
		</tr>

<% } %>
	</tbody>
</table>

<a href="employees_v2"  class="text-info"><span class="badge badge-pill badge-warning">View 2</span> </a>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

 <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
 	<script type="text/javascript">
 	 /*
		When opening the page, we will use the plug-in DataTable (see more: https://datatables.net/) to sort the table/ search for something faster.
		DataTables used to reduce the number of queries when sorting by another column.
		To be able to save the status of the table (the status here is saving which column selected and don't change it on reloading the page or when moving between pages),
		I used the boolean session 'startSession' that should be initialized when opening the home page or login (start the application) and it will be killed when logging out
		(closing the application). If the session for some reasons did not initialize the state of the table will be set to false (Which means re-set everything even on-loading).
		The session value will be collected from the method getSessionStatus()
	*/
 	$(document).ready(function() {
 	   $('#employeesTable').DataTable({
 		  "aLengthMenu": [[15, 30, 50, -1], [15, 30, 50, "All"]],
 		  'pageLength': 15,
 		  'language': {
 		    search: '',
 		    searchPlaceholder: "Search..."
 		  },
 		  'searching': true,
 		  'bLengthChange': true,
 		  'bPaginate': true,
 		  'order': [[4, 'asc'],[5, 'asc']],
 		  'paging': true,
 		   stateSave: getSessionStatus(),
 		
 		  'bInfo': false,
 		  'columnDefs': [{
 		    'orderable': false,
 		    'targets': 8
 		  }]
 		});

 	} );
 	 
 	 /*
 		Check if the session 'startSession' is set, returns true otherwise returns false.
	 */
 	function getSessionStatus(){
		<%
			String sessionValue = null;
			if(session.getAttribute("startSession")!=null){
				sessionValue = session.getAttribute("startSession").toString();
			}
		%> 
		var theSession = <%=sessionValue %>; 
		return theSession ? true : false;
	}
 	</script>
 
</body>
</html>