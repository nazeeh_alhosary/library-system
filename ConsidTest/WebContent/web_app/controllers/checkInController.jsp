<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Checkin Library Item";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>

<%@page errorPage ="error" %>
<jsp:useBean id="libraryItem" class="AppObjects.LibraryItem"></jsp:useBean>
<jsp:setProperty property="*" name="libraryItem"/>

<%
/*
	This controller used when the user check in a LibraryItem
	When the request is done, and there is not errors, it will redirect the pages to Library Items page
*/
	String id = request.getParameter("id");
	LibraryItemModel.checkIn(Integer.parseInt(id));
	response.sendRedirect("library_items");
%>
