<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Delete Employee";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>

<jsp:useBean id="employee" class="AppObjects.Employee"></jsp:useBean>
<jsp:setProperty property="*" name="employee"/>
<%@page errorPage ="employees_error" %>
<%
/*
	This controller used when the user delete an Employee 
	When the request is done, and there is not errors, it will redirect the pages to Employee page
*/
	String id = request.getParameter("id");
	EmployeeModel.delete(Integer.parseInt(id));
	response.sendRedirect("employees");
%>
