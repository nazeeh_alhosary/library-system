<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Delete Category";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>

<jsp:useBean id="category" class="AppObjects.Category"></jsp:useBean>
<jsp:setProperty property="*" name="category"/>
<%@page errorPage ="categories_error" %>
<%
/*
	This controller used when the user delete a Category 
	When the request is done, and there is not errors, it will redirect the pages to Categories page
*/
	String id = request.getParameter("id");
	CategoryModel.delete(Integer.parseInt(id));
	response.sendRedirect("categories");

%>
