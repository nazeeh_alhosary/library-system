<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Delete Library Item";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>

<jsp:useBean id="libraryItem" class="AppObjects.LibraryItem"></jsp:useBean>
<jsp:setProperty property="*" name="libraryItem"/>
<%@page errorPage ="library_items_error" %>
<%
/*
	This controller used when the user delete a Library Item 
	When the request is done, and there is not errors, it will redirect the pages to Library Item page
*/
	String id = request.getParameter("id");
	LibraryItemModel.delete(Integer.parseInt(id));
	response.sendRedirect("library_items");
%>
