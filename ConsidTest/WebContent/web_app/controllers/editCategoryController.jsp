<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Edit Category";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>

<jsp:useBean id="category" class="AppObjects.Category"></jsp:useBean>
<jsp:setProperty property="*" name="category"/>

<%
/*
	This controller used when the user edit a Category
	When the request is done, and there is not errors, it will redirect the pages to Category page
*/
	int num = CategoryModel.update(category);
	response.sendRedirect("categories");
%>

