<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Edit Employee";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>

<jsp:useBean id="employee" class="AppObjects.Employee"></jsp:useBean>
<jsp:setProperty property="*" name="employee"/>
<%@page errorPage ="error" %>

<%
/*
	This controller used when the user edit an Employee
	When the request is done, and there is not errors, it will redirect the pages to Employee page
*/
//Get the boolean Variable from the post and assign them to the employee
	if(request.getParameter("isCEO") !=null){
		employee.setIsCEO(true);
	}else{
		employee.setIsCEO(false);
	}
	
	if(request.getParameter("isManager") !=null){
		employee.setIsManager(true);
	}else{
		employee.setIsManager(false);
	}
	int num = EmployeeModel.update(employee);
	response.sendRedirect("employees");
%>

