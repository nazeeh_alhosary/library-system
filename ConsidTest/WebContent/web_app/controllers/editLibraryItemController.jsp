<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	String title = "Edit Category";
	request.setAttribute("pageTitle", (Object)title);
%>


<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>

<jsp:useBean id="libraryItem" class="AppObjects.LibraryItem"></jsp:useBean>
<jsp:setProperty property="*" name="libraryItem"/>
<%@page errorPage ="error" %>

<%
/*
	This controller used when the user edit a Library item
	When the request is done, and there is not errors, it will redirect the pages to LibraryItems page
*/
int num = LibraryItemModel.update(libraryItem);
response.sendRedirect("library_items");
%>

