<br>
<hr >
</div>
<footer class="page-footer font-small blue">
<!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <p>Nazeeh Alhosary&copy; 2020 | Test from <a href="https://consid.se/" target="_blank"><img  src="web_app/public/img/consid_logo.png" width="100" height="40" alt="Card image cap"></a> <b>AB</b></p>
        
    </div>
</footer>
	<script src="web_app/public/js/main.js" type="text/javascript"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
/*
	When clicking on the logout button, show the user the sweetalert. 
	If the user clicke 'Logout' it will call the method redirectTo()
	If the user clicks 'Cancel' it will close the alert
 */
$( '#logout' ).on('click',function() {
		   swal({
			   title: "Logout!",
			   text: "Are you sure you want to logout?",
			   icon: "warning",
			   buttons: ["Cancel", "Logout"],
			   dangerMode: true,
			 })
			 .then((willDelete) => {
			   if (willDelete) {	
				   redirectTo('/logout');
			   }
			 });
});

/* 
	The method redirectTo(urlPattern):
	Since all used URLs declared in the web.xml file in the same way (projectName/URL), we can redirect to the wanted page without any problem		
	To avoid hardcoded the project name, we getPath from the browser URL which is the project name, then redirect to the urlPattern page
*/
function redirectTo(urlPattern){
	<% String getPath = request.getContextPath(); %> 
	var path = "<%=getPath %>"; 
	window.location.href = path + urlPattern;
}

</script>
