<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <% 
    	//Get the title from the page
    	String pageTitle = (String) request.getAttribute("pageTitle");
    %>
	<title>Nazeeh | <%= pageTitle %> </title>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="web_app/public/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>
<body>

<!-- Start Nav-Bar  -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<a class="navbar-brand" href="<%= request.getContextPath() %> ">
	    <img src="web_app/public/img/books3.svg" width="35" height="35" class="d-inline-block align-top" alt="" > CONSID
	  </a>
	 <!--  <a class="navbar-brand" href="#">Consid</a> -->
	  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="navbar-collapse collapse" id="navbarColor01" style="">
	   
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="<%= request.getContextPath() %> " >Home </a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="library_items" >Library Items</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="categories">Categories</a>
	      </li>
	      
		  <li class="nav-item">
	        <a class="nav-link" href="employees">Employees</a>
	      </li>
	    </ul>
	    <dive class="form-inline my-2 my-lg-0">
	    	<button class="btn btn-danger my-2 my-sm-0" id="logout"><span class="fas fa-power-off"></span>&ensp;Logout</button>
   		 </div>
	  </div>
	</nav>
<!-- End Nav-Bar  -->

<div class="container">	
