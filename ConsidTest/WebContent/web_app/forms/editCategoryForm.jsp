<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Edit Category";
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>

<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>

<%
/*
	Get the category id from the request.
	Fetch the category information from the database using the method getOneRecord() to show it to the user when editting.
*/
	String id = request.getParameter("id");
	Category category = CategoryModel.getOneRecord(Integer.parseInt(id));
%>
<div class="card mt-4">
  <div class="card-header">
    Edit Category
  </div>
  <div class="card-body">
	<form action="edit_category_controller" method="POST" class="needs-validation" novalidate>
			
		<div class="form-group">
	      <input type="hidden" class="form-control" id="id" name="id" value='<%= category.getId() %>' >
	    </div>
		<div class="form-group">
	      <label for="CategoryName">Category Name</label>
	      <input type="text" class="form-control" name="categoryName" value='<%= category.getCategoryName() %>' >
	    </div>
	    
	    <button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-edit"></span>&ensp;Edit Category</button>	
	    <a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
	    	    
	</form>
	</div>
</div>


<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

<script>
//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>	
</body>
</html>
