<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@page import="java.util.List" %>
<%
//list: is a list of categories from the database to show it to the user
	List<Category> list = CategoryModel.getAllRecords();
%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Library Item Form</title>
</head>
<body>

<div class="card mt-4">
	<div class="card-header">Add New Library Item</div>
		<div class="card-body">
			<form action="add_library_item" method="POST" class="needs-validation" novalidate>	
				<div class="form-group">
						<label for="exampleSelect1">Type <span style="color:red;"> *</span> </label> 
						
						<div class="addLibraryItemForm_selectType">		
						<select class="form-control" id="type" name="type" required>
							<option value="" selected disabled>Select Type ...</option>
							<option value="Book">Book</option>
							<option value="DVD">DVD</option>
							<option value="Audio Book">Audio Book</option>
							<option value="Reference Book">Reference Book</option>
						</select>
						</div>
					</div>
				
					<div class="form-group">
						<label for="categoryName">Title<span style="color:red;"> *</span> </label>
						 <input type="text" class="form-control" id="title" name="title" placeholder="Enter a new Item Title" >
					</div>
				
					<div class="form-group" id="div_author">
						<label for="categoryName">Author<span style="color:red;"> *</span>  </label>
						 <input type="text" class="form-control" id="author" name="author" placeholder="Enter the Author name">
					</div>
	
				
					<div class="form-group">
						<label for="exampleSelect1">Category<span style="color:red;"> *</span> </label> <select
							class="form-control" id="categoryId" name="categoryId" required>
							<option value="" selected disabled>Select Category ...</option>
							<% for(Category category:list){ %>
								<option value="<%= category.getId() %>"><%= category.getCategoryName() %></option>
							<% } %>
							
						</select>
					</div>
	
					<div class="form-group" id="div_pages">
						<label class="control-label">Pages<span style="color:red;"> *</span> </label> 
						<div class="input-group mb-3">
							<input type="number" class="form-control" id="pages" name="pages" placeholder="Enter number of pages" min="0">
						<div class="input-group-append">
							<span class="input-group-text">Page(s)</span>
						</div> 
						</div> 
					</div>
	
					<div class="form-group" id="div_runTimeMinutes" style="display: none">
						<label class="control-label">RunTimeMinutes<span style="color:red;"> *</span> </label>
						<div class="input-group mb-3">
							<input type="number" class="form-control" id="runTimeMinutes" name="runTimeMinutes" placeholder="Enter run time per minutes" min="0">
						<div class="input-group-append">
							<span class="input-group-text">Minute(s)</span>
						</div> 
						</div> 
					</div>
					
					<div class="form-group">
						<div class="custom-control custom-switch">
							<input type="checkbox" class="custom-control-input" id="isBorrowable" name="isBorrowable" checked=""> 
								<label class="custom-control-label" for="isBorrowable">Is Borrowable?<span style="color:red;"> *</span> </label>
						</div>
					</div>
	
					<div class="form-group"  id="div_borrower" style="display: none">
						<label for="borrower">Borrower <span style="color:red;"> *</span> </label>
						 <input type="text" class="form-control" id="borrower" name="borrower" placeholder="Enter the Borrower Name" >
						 <small id="emailHelp" class="form-text text-muted">Note: The Borrow date will be set to the current date.</small>
					</div>
				
					<small id="emailHelp" class="form-text text-muted"><span style="color:red;">*</span> Required field.</small>
					<button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-folder-plus"></span>&ensp;Add Item </button>
					<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
			</form>
		</div>		
	</div>
</div>
	
<script>
//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>	
</body>
</html>