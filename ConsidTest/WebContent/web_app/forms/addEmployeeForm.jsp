<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>

<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@page import="java.util.List" %>

<%
	//Get a list of all managers and the CEO when adding a new employee
	List<Employee> possibleManagers = EmployeeModel.getAllManagers();
%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Library Item Form</title>
</head>
<body>

	<div class="card mt-4">
		<div class="card-header">
			Add New Employee
		</div>
		<div class="card-body">
			<form action="add_employee" method="POST" class="needs-validation" novalidate>
				
				<div class="form-group">
					<label for="firstName">First Name<span style="color:red;"> *</span> </label>
					<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter a new Item Title" required >
					<!-- <div class="invalid-feedback">Name can't start with space</div> -->
				</div>
				
				<div class="form-group">
					<label for="lastName">Last Name<span style="color:red;"> *</span>  </label>
					<input type="text" class="form-control" id=lastName name="lastName" placeholder="Enter the Author name" required>
				</div>

				<div class="form-group" id="div_salary">
						<label class="control-label">Rank<span style="color:red;"> *</span> </label> 
						<div class="input-group mb-3">				
						<input type="number" class="form-control" id="rank" name="rank" placeholder="Enter Rank between 1-10" min="1"  max="10" required>
							<div class="input-group-append">
								<span class="input-group-text">Rank</span>
							</div> 
						</div> 
				</div>
					
 				<div class="form-group isCEO">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="isCEO" name="isCEO"> 
						<label class="custom-control-label" for="isCEO">Is CEO?<span style="color:red;"> *</span> </label>
					</div>
				</div>				

				<div class="form-group isManager">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="isManager" name="isManager"> 
						<label class="custom-control-label" for="isManager">Is Manager?<span style="color:red;"> *</span> </label>
					</div>
				</div>			
								
				<div class="form-group " id="div_managerId">
					<label for="managerId">Has Manager?<span style="color:red;"></span> </label> 
					<select class="form-control" id="managerId" name="managerId" required>
						<option value="" selected disabled>Select Manager ...</option>
						<% for(Employee manager:possibleManagers){ %>
							<%
							/* 
								The CEO option will be hidden if the user is a regular employee else it will be displayed
								Displaying and hiding the CEO will be through JS code in the main.js file
							**/
							if(manager.isIsCEO()){%>
								<option id="optionCEO" value="<%= manager.getId()%>"> ID: <%= manager.getId() %>, Name:<%= manager.getFirstName()%> <%=manager.getLastName()%> | Role: <%=manager.getRole()%> </option>
							<% }else{ %>
								<option value="<%= manager.getId() %>">ID: <%= manager.getId() %>, Name: <%= manager.getFirstName()%> <%=manager.getLastName()%> | Role: <%=manager.getRole()%> </option>
							<% } %>							
						<% } %>
					</select>
				</div>
				
				<!-- The following hidden select used to avoid any error when there is no CEO in the DB  -->
				<select class="form-control" style="display:none;" >
						<option id="optionCEO" value=""></option>
				</select>
				
				<small id="emailHelp" class="form-text text-muted"><span style="color:red;">*</span> Required field.</small>
				<button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-user-plus"></span>&ensp;Add Employee </button>
				<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>	
			</form>
		</div>
	</div>
	
<script>
//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</body>
</html>