<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Edit Employee";
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<%@page errorPage ="error" %>
<%@page import="AppModels.EmployeeModel" %>
<%@page import="AppObjects.Employee" %>

<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@page import="java.util.List" %>
<%
/*
	Get the employee id from the request.
	Fetch the employee information from the database using the method getOneRecord() to show it to the user when editting.
	Fetch all possible managers that can manage the selected employee in case the user want to change the employee's manager.
*/
	String id = request.getParameter("id");
	Employee employee = EmployeeModel.getOneRecord(Integer.parseInt(id));
	List<Employee> possibleManagers = EmployeeModel.getPossiableManagers( employee.getId(),true);
%>

<div class="card mt-4">
	<div class="card-header">
		Edit Employee
	</div>
	<div class="card-body">
		<form action="edit_employee_controller" method="POST" class="needs-validation" novalidate >
			<div class="form-group">
			      <input type="hidden" class="form-control" id="id" name="id" value='<%= employee.getId() %>' >
		    </div>			
			<div class="form-group">
				<label for="firstName">First Name<span style="color:red;"> *</span> </label>
				<input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter a new Item Title" value="<%= employee.getFirstName() %>" required >
			</div>
			
			<div class="form-group">
				<label for="lastName">Last Name<span style="color:red;"> *</span>  </label>
				<input type="text" class="form-control" id=lastName name="lastName" placeholder="Enter the Author name" value="<%= employee.getLastName() %>"required>
			</div>

			<div class="form-group" id="div_salary">
					<label class="control-label">Rank<span style="color:red;"> *</span> </label> 
					<div class="input-group mb-3">				
						<input type="number" class="form-control" id="rank" name="rank" placeholder="Enter Rank between 1-10" min="1"  max="10" value="<%= employee.getRank()%>"required>
						<div class="input-group-append">
							<span class="input-group-text">Rank</span>
						</div> 
					</div> 
			</div>

 				<div class="form-group" id="div_salary">
					<label class="control-label">Salary<span style="color:red;"></span> </label> 
					<div class="input-group mb-3">
					   <div class="input-group-prepend">
					        <span class="input-group-text">&euro;</span>
					   </div>					
					<input type="number" class="form-control" id="salary" name="salary" placeholder="Enter Salary" min="0"  value="<%= employee.getSalary()%>" readonly>
					<div class="input-group-append">
						<span class="input-group-text">K</span>
					</div> 
					</div> 
				</div>
				
			<div class="form-group isCEO">
				<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="isCEO" name="isCEO" <%if(employee.isIsCEO()){%> checked<% }else{ %> disabled <% } %>> 
						<label class="custom-control-label" for="isCEO">Is CEO?<span style="color:red;"> *</span> </label>
				</div>
			</div>				

			<div class="form-group isManager">
				<div class="custom-control custom-switch">
					
						<input type="checkbox" class="custom-control-input" id="isManager" name="isManager" <%if(employee.isIsManager()){%> checked<% }else{ %> disabled <% } %> > 
						<label class="custom-control-label" for="isManager">Is Manager?<span style="color:red;"> *</span> </label>
					
				</div>
			</div>			
							
			<div class="form-group " id="div_managerId">
				<label for="managerId">Has Manager?<span style="color:red;"></span> </label> 
				<select class="form-control" id="managerId" name="managerId" required>
					<option value="" selected disabled>Select Manager ...</option>
					
					<% for(Employee manager:possibleManagers){ %>
							<%
							/* 
								The CEO option will be hidden if the user is a regular employee else it will be displayed
								Displaying and hiding the CEO will be through JS code in the main.js file
							**/
							if(manager.isIsCEO()){%>
							<option id="optionCEO" <% if(manager.getId() == employee.getManagerId()) {%> selected <% } %> value="<%= manager.getId()%>"> ID: <%= manager.getId() %>, Name:<%= manager.getFirstName()%> <%=manager.getLastName()%> | Role: <%=manager.getRole()%> </option>
						<% }else{ %>
								<option <% if(manager.getId() == employee.getManagerId()) {%> selected <% } %> value="<%= manager.getId() %>">ID: <%= manager.getId() %>, Name: <%= manager.getFirstName()%> <%=manager.getLastName()%> | Role: <%=manager.getRole()%> </option>
						<% } %>
					<% } %>
				</select>
			</div>
				
				<!-- The following hidden select used to avoid any error when there is no CEO in the DB  -->
				<select class="form-control" style="display:none;" name="hiddenOption">
						<option id="optionCEO" value=""></option>
				</select>			
			
			<small id="emailHelp" class="form-text text-muted"><span style="color:red;">*</span> Required field.</small>
			<button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-user-edit"></span>&ensp;Edit Employee </button>
			<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
		</form>
	</div>
</div>

	
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>
<script>
//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

</body>
</html>