<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Edit Library Item";
request.setAttribute("pageTitle", (Object)title);
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<%@page errorPage ="error" %>
<%@page import="AppModels.CategoryModel" %>
<%@page import="AppObjects.Category" %>
<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@page import="java.util.List" %>
<%
/*
	Fetch all all categories from the database in case the user want to change the item's category name.
	Get the libraryItem id from the request.
	Fetch the libraryItem information from the database using the method getOneRecord() to show it to the user when editting.
*/
	List<Category> list = CategoryModel.getAllRecords();
	String id = request.getParameter("id");
	LibraryItem libraryItem = LibraryItemModel.getOneRecord(Integer.parseInt(id));
%>

<div class="d-flex flex-row-reverse bd-highlight mt-4">
	<a href="checkout?id=<%= libraryItem.getId() %>" role="button" class="btn btn-warning btn-lg" <% if(!libraryItem.isIsBorrowable()){ %> style="display: none;" <% } %> > <span class="fas fa-sign-out-alt"></span>&ensp;Check Out</a>
	<a href="checkin_controller?id=<%= libraryItem.getId() %>"  role="button" class="btn btn-primary btn-lg" <% if(libraryItem.isIsBorrowable()){ %> style="display: none;" <% } %>> <span class="fas fa-sign-in-alt"></span>&ensp;Check In</a>
</div>

<div class="card mt-2">
  <div class="card-header">
    Edit Library Item
  </div>
  <div class="card-body">
	<form action="edit_library_item_controller" method="POST" class="needs-validation" novalidate>
		<div class="form-group">
	      <input type="hidden" class="form-control" id="id" name="id" value='<%= libraryItem.getId() %>' >
	    </div>
		<div class="form-group">
			<label for="exampleSelect1">Type</label> 	
			<div class="addLibraryItemForm_selectType">		
			<select class="form-control" id="type" name="type" required>
				<option value="" disabled>Select Type ...</option>
				<option <% if (libraryItem.getType().equalsIgnoreCase("Book")){ %> selected="selected" <%} %> value="Book">Book</option>
				<option <% if (libraryItem.getType().equalsIgnoreCase("DVD")){ %> selected="selected" <%} %> value="DVD">DVD</option>
				<option <% if (libraryItem.getType().equalsIgnoreCase("Audio Book")){ %> selected="selected" <%} %> value="Audio Book">Audio Book</option>
				<option <% if (libraryItem.getType().equalsIgnoreCase("Reference Book")){ %> selected="selected" <%} %> value="Reference Book">Reference Book</option>
			</select>
			</div>
		</div>
			
			<div class="form-group">
				<label for="categoryName">Title</label>
				 <input type="text" class="form-control" id="title" name="title" placeholder="Enter a new Item Title" value="<%= libraryItem.getTitle() %>">
			</div>
			
			<div class="form-group" id="div_author">
				<label for="categoryName">Author </label>
				 <input type="text" class="form-control" id="author" name="author" placeholder="Enter the Author name" value="<%= libraryItem.getAuthor() %>"> 
			</div>
	
			
			<div class="form-group">
				<label for="exampleSelect1">Category</label> <select
					class="form-control" id="categoryId" name="categoryId" required>
					<option value="" selected disabled>Select Category ...</option>
					<% for(Category category:list){ %>
						<option <% if(libraryItem.getCategoryName().equalsIgnoreCase(category.getCategoryName())) {%> selected <% } %>value="<%= category.getId() %>"><%= category.getCategoryName() %></option>
					<% } %>
					
				</select>
			</div>
	
			<div class="form-group" id="div_pages">
				<label class="control-label">Pages:</label> 
				<div class="input-group mb-3">
					<input type="number" class="form-control" id="pages" name="pages" placeholder="Enter number of pages" min="0" value="<% if(libraryItem.getPages()>0){%><%=libraryItem.getPages()%><%}%>">
				<div class="input-group-append">
					<span class="input-group-text">Page(s)</span>
				</div> 
				</div> 
			</div>
	
			<div class="form-group" id="div_runTimeMinutes" style="display: none">
				<label class="control-label">RunTimeMinutes:</label>
				<div class="input-group mb-3">
					<input type="number" class="form-control" id="runTimeMinutes" name="runTimeMinutes" placeholder="Enter run time per minutes" min="0" value="<% if(libraryItem.getRunTimeMinutes()>0){%><%=libraryItem.getRunTimeMinutes()%><%}%>">
				<div class="input-group-append">
					<span class="input-group-text">Minute(s)</span>
				</div> 
				</div> 
			</div>
	
			
			<div class="form-group" style="display: none">
				<div class="custom-control custom-switch">
					<input type="checkbox" class="custom-control-input" id="isBorrowable" name="isBorrowable" checked=""> 
						<label class="custom-control-label" for="isBorrowable">Is Borrowable?</label>
				</div>
			</div>
	
			<div class="form-group"  id="div_borrower" style="display: none">
				<label for="borrower">Borrower</label>
				 <input type="text" class="form-control" id="borrower" name="borrower" placeholder="Enter the Borrower Name" value="<%= libraryItem.getBorrower() %>">
			</div>
		
		<button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-edit"></span>&ensp;Edit Item </button>
		<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
		
	</div>
	
</form>

</div>
</div>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

<script>
//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>	

</body>
</html>
