<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%
	String title = "Checkout Library Item";
	request.setAttribute("pageTitle", (Object)title);
%> 
<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<%@page import="javax.servlet.http.*" %>
<%@page import="javax.servlet.*" %>
<%@page import="AppModels.LibraryItemModel" %>
<%@page import="AppObjects.LibraryItem" %>
<%@page errorPage ="error" %>
<%
	String id = request.getParameter("id");
LibraryItem libraryItem = LibraryItemModel.getOneRecord(Integer.parseInt(id));
%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Check Out</title>
</head>
<body>

	<div class="card mt-4">
		<div class="card-header">Check Out Form for a Library Item</div>
		<div class="card-body">
			<form action="checkout_controller" method="POST" class="needs-validation" novalidate>
			
				<div class="form-group">
					<input type="hidden" class="form-control" id="id" name="id" value='<%= libraryItem.getId() %>' >
				</div>
				<div class="card bg-light mb-3" style="max-width: 20rem;">
				 <div class="card-header">Check out a <%= libraryItem.getType() %></div>
					 <div class="card-body">
					    <h4 class="card-title"> <b> Title: </b><%= libraryItem.getTitle() %></h4>
				   		 <p class="card-text"><em>Category: <%= libraryItem.getCategoryName() %></em></p>
			  		</div>
				</div>
				<div class="form-group"  id="div_borrower">
					<label for="borrower">Borrower <span style="color:red;">*</span></label>
					 <input type="text" class="form-control" id="borrower" name="borrower" placeholder="Enter the Borrower Name" required>
					 <small id="emailHelp" class="form-text text-muted">Note: The Borrow date will be set to the current date.</small>
					 				 
				</div>
				<small id="emailHelp" class="form-text text-muted"><span style="color:red;">*</span> Required field.</small>	
				<button type="submit" class="btn btn-success float-right btn-lg " <%if(!libraryItem.isIsBorrowable()){ %>  style="display: none;" <%} %>> <span class="fas fa-sign-out-alt"></span>&ensp;Check Out</button>
				<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
				
		</div>


		
		</form>

	</div>
	</div>
<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>
<script>
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>	
</body>
</html>
