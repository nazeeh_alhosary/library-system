<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Category Form</title>
</head>
<body>

<div class="card mt-4">
  <div class="card-header">
    Add New Category
  </div>
  <div class="card-body">
		<form action="add_category" method="POST" class="needs-validation" novalidate>
		    <div class="form-group">
		      <label for="categoryName">Category Name <span style="color:red;">*</span></label>
		      <input type="text" class="form-control" name="categoryName" placeholder="Enter a new Category Name" required>
		    </div>  
		    <small id="emailHelp" class="form-text text-muted"><span style="color:red;">*</span> Required field.</small>
		    <button type="submit" class="btn btn-success float-right btn-lg"> <span class="fas fa-folder-plus"></span>&ensp;Add Category</button>
			<a href="javascript:history.back()" role="button" class="btn btn-danger float-right btn-lg mr-3"> <span class="fas fa-times"></span>&ensp;Cancel</a>
		    
		</form>

  </div>
</div>
<script>

//For Validate the form, using the Bootstrap validator
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</body>
</html>