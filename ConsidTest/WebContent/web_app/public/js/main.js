//Get the required fields when open the page edit_library_item
if(document.location.pathname.includes('edit_library_item')){
	var selectedType = document.getElementById('type').value;
	setRequiredFields(selectedType);
}

//Get the required fields when open the page edit_library_item or add_employee
if(document.location.pathname.includes('edit_employee') || document.location.pathname.includes('add_employee')){
	setEmployeeRequiredFields();
}

/*
 * The following two methods setCEORequiredFields() and setManagerRequiredFields() deal with the switches on the Add New Employee page
 * If the user clicks on is CEO, the first method will make clicking on is Manage disabled 
 * Same thing on the second method if the user clicks on is Manager will make clicking on is CEO disabled.
 * The method will be called when clicking on the switches (isCEO or isManager)
*/
$('.isCEO').on( 'change', function(e) {
	setCEORequiredFields();
});

$('.isManager').on( 'change', function(e) {
	setManagerRequiredFields();
});

function setCEORequiredFields(){
	var isCEO = document.getElementById('isCEO').checked ;
	if(isCEO){
		document.getElementById('isManager').setAttribute('disabled', "");
		document.getElementById('managerId').removeAttribute('required');
		document.getElementById('div_managerId').style.display = 'none';		
	}else{
		 document.getElementById('isManager').removeAttribute('disabled');
		 document.getElementById('div_managerId').style.display = 'block';
		 document.getElementById('managerId').setAttribute('required', true);
		 document.getElementById('optionCEO').style.display = 'none';
	}
}
function setManagerRequiredFields(){
	var isManager = document.getElementById('isManager').checked;
	if(isManager){
		document.getElementById('isCEO').setAttribute('disabled', "");
		document.getElementById('managerId').removeAttribute('required');
		document.getElementById('optionCEO').style.display = 'block';
	}else{
		document.getElementById('isCEO').removeAttribute('disabled');
		document.getElementById('managerId').setAttribute('required', true);
		document.getElementById('optionCEO').style.display = 'none';
	}
}
//setEmployeeRequiredFields() behavior is the same as the previous functions, but it will be used when opening the page.
function setEmployeeRequiredFields(){
	var isCEO = document.getElementById('isCEO').checked;
	var isManager = document.getElementById('isManager').checked;
	if(isCEO){
		document.getElementById('isManager').setAttribute('disabled', "");
		document.getElementById('managerId').removeAttribute('required');
		document.getElementById('div_managerId').style.display = 'none';
	}else if (isManager){
		document.getElementById('isCEO').setAttribute('disabled', "");
		document.getElementById('managerId').removeAttribute('required');
		document.getElementById('optionCEO').style.display = 'block';
	}else{
		document.getElementById('isCEO').removeAttribute('disabled');
		document.getElementById('isManager').removeAttribute('disabled');
		document.getElementById('managerId').setAttribute('required', true);
		document.getElementById('optionCEO').style.display = 'none';
	}
}


//When Click on Select Manager, the following method will hide the CEO if the user adding normal employee
$('#managerId').on( 'click', function(e) {
	var isCEO = document.getElementById('isCEO').checked;
	var isManager = document.getElementById('isManager').checked;
	if(!isCEO && !isManager){
		document.getElementById('optionCEO').style.display = 'none';
	}else{
		document.getElementById('optionCEO').style.display = 'block';
	}

});


//When the user in the Add Library page click on the drop down menu to select a type, it hide/display the required filed for each type.
$('.addLibraryItemForm_selectType').on( 'change', function(e) {
	//console.log(document.location.pathname);
	var selectedType = document.getElementById('type').value;
	console.log(selectedType);
	setRequiredFields(selectedType);
	});

function setRequiredFields(selectedType){
	document.getElementById('title').setAttribute('required', true);
	document.getElementById('categoryId').setAttribute('required', true);
	switch (selectedType) {
	
		case 'Book':
			// Show the Required field
			document.getElementById('div_pages').style.display = 'block';
			document.getElementById('div_author').style.display = 'block';
		
			// Add Required attributes
			document.getElementById('pages').setAttribute('required', true);
			document.getElementById('author').setAttribute('required', true);
			document.getElementById('isBorrowable').setAttribute('checked', "");
		
			// Hide the not Required field
			document.getElementById('div_runTimeMinutes').style.display = 'none';
		
			// Remove others' required attributes
			document.getElementById('isBorrowable').removeAttribute('disabled');
			document.getElementById('runTimeMinutes').removeAttribute('required');
			break;
		
		case 'DVD':
		case 'Audio Book':
			// Show the Required field
			document.getElementById('div_runTimeMinutes').style.display = 'block';
		
			// Add Required attributes
			document.getElementById('runTimeMinutes').setAttribute('required', true);
			document.getElementById('isBorrowable').setAttribute('checked', "");
		
			// Hide the not Required field
			document.getElementById('div_pages').style.display = 'none';
			document.getElementById('div_author').style.display = 'none';
		
			// Remove others required
			document.getElementById('isBorrowable').removeAttribute('disabled');
			document.getElementById('pages').removeAttribute('required');
			document.getElementById('div_author').removeAttribute('required');
			break;
		
		case 'Reference Book':
			// Show the Required field
			document.getElementById('div_pages').style.display = 'block';
			document.getElementById('div_author').style.display = 'block';
		
			// Add Required attributes
			document.getElementById('pages').setAttribute('required', true);
			document.getElementById('author').setAttribute('required', true);
			document.getElementById('isBorrowable').setAttribute('disabled', "");
		
			// Hide the not Required field
			document.getElementById('div_runTimeMinutes').style.display = 'none';
			document.getElementById('div_borrower').style.display = 'none';
		
			// Remove others' required attributes
			document.getElementById('isBorrowable').removeAttribute('checked');
			document.getElementById('runTimeMinutes').removeAttribute('required');
			
	
			document.getElementById('borrower').removeAttribute('required');
			document.getElementById('borrower').removeAttribute('required');
				break;
	}
}


//When Click on isBorrowable switch, the following method will make the Borrower name required or not.
$('#isBorrowable').on( 'change', function(e) {
	 var check = document.getElementById('isBorrowable').checked ? 1:0;
	 console.log('The checkbox is: ' +check);
	 if(check === 0){
		document.getElementById('div_borrower').style.display = 'block';
		document.getElementById('borrower').setAttribute('required', true);
	 }else{
		document.getElementById('div_borrower').style.display = 'none';
		document.getElementById('borrower').removeAttribute('required');
	 }
});



