<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% 
String title = "Home";
Object  obj = title;
request.setAttribute("pageTitle", (Object)title);
%>
<%
//If the session not set in the login page.
if(session.getAttribute("startSession") ==null){
	session.setAttribute("startSession",true);
}
%>

<jsp:include page="/web_app/inc/header.jsp"></jsp:include>
<!-- Start jumbotron -->
	<div class="jumbotron text-center">
	  <div class="container">
	    <h1 class="display-4">WEB EXERCISE!</h1>
	    <p class="lead">FOR BACKEND DEVELOPERS | V2.0</p>
	    <hr class="my-4">
		  <p class="d-flex justify-content-center font-italic">See more about  <a class="font-weight-bold" href="https://consid.se/" target="_blank"> Consid AB </a>  in our home page.</p>
		  
		   <div class="d-flex justify-content-center">
		  	<a href="library_items" role="button" class="btn btn-warning btn-lg mr-3"> <span class="fas fa-clipboard-list"> </span> Library Items</a>
		  </div> 
		 <br>
		  <div class="d-flex justify-content-center">
		  	<a href="categories" role="button" class="btn btn-primary btn-lg mr-3"> <span class="fas fa-layer-group" ></span> Categories</a>
  		  	<a href="employees" role="button" class="btn btn-success btn-lg ml-3"> <span class="fas fa-users" href="viewStudents.jsp"></span> Employees</a>	  	
	  	 </div>
	  	 
	  </div>
	</div>
<!-- End jumbotron -->
<div class="row">

<div class="col-xs-6 col-md-4">
	<div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
	  <div class="card-header">Category</div>
	  <div class="card-body">
	    <h4 class="card-title">Primary card title</h4>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	  </div>
	</div>
</div>

<div class="col-xs-6 col-md-4">
	<div class="card  bg-light mb-3" style="max-width: 20rem;">
	  <div class="card-header">Library Item</div>
	  <div class="card-body">
	    <h4 class="card-title">Secondary card title</h4>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	  </div>
	</div>
</div>

<div class="col-xs-6 col-md-4">
	<div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
	  <div class="card-header">Employee</div>
	  <div class="card-body">
	    <h4 class="card-title">Primary card title</h4>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	  </div>
	</div>
</div>


</div>

<jsp:include page="/web_app/inc/footer.jsp"></jsp:include>

</body>
</html>


