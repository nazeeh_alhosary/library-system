package Config;
/*
 * The Object class DBConnection is an Object that make the connection with the Database. 
 * */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	public static Connection getConnection() throws Exception {
		Connection conn = null;
		String dbName = "TestConsid"; //Your Database name
		String dbUserName= "root"; //Your host user name (e.g., root)
		String dbPassWord ="NazeeH12345@"; // Your host password (e.g, "")
		String dbHost = "localhost"; // Your HOST (localhost if loacl)
		String dbPort = "3306";// Your Database PORT
		String url= "jdbc:mysql://" + dbHost +":"+dbPort+"/"+dbName;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url,dbUserName,dbPassWord);
		} catch (ClassNotFoundException | SQLException e) {
			throw new Exception(e);
			//e.printStackTrace();
		}
		return conn;
	}
}
