package AppObjects;
/*
 * The Object class Category is an Object that represents the behaviors of the Category Table in the database. 
 * We could have a constructor to initialize the variable when using this object, but no need for the constructor
 * in our case because we do everything in the Models when using the Class
 * */
public class Category {

	private int Id;
	private String CategoryName;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getCategoryName() {
		return CategoryName;
	}
	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}
	
}
