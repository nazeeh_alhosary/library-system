package AppObjects;
/*
 * The Object class Employee is an Object that represents the behaviors of the Employees Table in the database. 
 * We could have a constructor to initialize the variable when using this object, but no need for the constructor
 *  in our case because we do everything in the Models when using the Class
 * */
public class Employee {
	
	private int Id;
	private String FirstName;
	private String LastName;
	private double Salary;
	private boolean IsCEO;
	private boolean IsManager;
	private int ManagerId;
	
	/*
	 * Extra three variables used to make the use of this Object easier and to reduce the number of queries every time we want to use this Object.
	 * role: To save the role of the employee as String.
	 * rank: To save the input rank to calculate the user Salary
	 * managerFullName: to save the manager full name when using the first view.
	 * */
	private String role;
	private int rank;
	private String managerFullName;
	
	public String getManagerFullName() {
		return managerFullName;
	}
	public void setManagerFullName(String managerFullName) {
		this.managerFullName = managerFullName;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	//
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public double getSalary() {
		return Salary;
	}
	public void setSalary(double salary) {
		Salary = salary;
	}
	public boolean isIsCEO() {
		return IsCEO;
	}
	public void setIsCEO(boolean isCEO) {
		IsCEO = isCEO;
	}
	public boolean isIsManager() {
		return IsManager;
	}
	public void setIsManager(boolean isManager) {
		IsManager = isManager;
	}
	public int getManagerId() {
		return ManagerId;
	}
	public void setManagerId(int managerId) {
		ManagerId = managerId;
	}

}
