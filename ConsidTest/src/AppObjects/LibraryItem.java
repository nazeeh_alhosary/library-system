package AppObjects;
/*
 * The Object class LibraryItem is an Object that represents the behaviors of the LibraryItem Table in the database. 
 * We could have a constructor to initialize the variable when using this object, but no need for the constructor
 *  in our case because we do everything in the Models when using the Class
 * */
public class LibraryItem {

	private int Id;
	private int CategoryId;
	private String Title;
	private String Author;
	private int Pages;
	private int RunTimeMinutes;
	private boolean IsBorrowable;
	private String Borrower;
	private String BorrowDate;
	private String Type;
	private String CategoryName; 
	

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(int categoryId) {
		CategoryId = categoryId;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getAuthor() {
		return Author;
	}
	public void setAuthor(String author) {
		Author = author;
	}
	public int getPages() {
		return Pages;
	}
	public void setPages(int pages) {
		Pages = pages;
	}
	public int getRunTimeMinutes() {
		return RunTimeMinutes;
	}
	public void setRunTimeMinutes(int runTimeMinutes) {
		RunTimeMinutes = runTimeMinutes;
	}
	public boolean isIsBorrowable() {
		return IsBorrowable;
	}
	public void setIsBorrowable(boolean isBorrowable) {
		IsBorrowable = isBorrowable;
	}
	public String getBorrower() {
		return Borrower;
	}
	public void setBorrower(String borrower) {
		Borrower = borrower;
	}
	public String getBorrowDate() {
		return BorrowDate;
	}
	public void setBorrowDate(String borrowDate) {
		BorrowDate = borrowDate;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getCategoryName() {
		return CategoryName;
	}
	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}
	
}
