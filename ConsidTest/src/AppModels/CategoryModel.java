package AppModels;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import AppObjects.Category;
import Config.DBConnection;

/*
 * The class CategoryModel is a Model that will deal with the Object Category to avoid having the Object 
 * behaviors and The Database Connection in one Class, and to separate the classes according to their behaviors. 
 * */
public class CategoryModel {
	/*
	 * @param (Category)
	 * The method insert is responsible for adding a new category of the category name not exist
	 *  it will be added to the database otherwise it will show an error message to the end-user. 
	 * */
	//Add new record
	public static int insert(Category category) throws IllegalAccessException {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "INSERT INTO Category (CategoryName) VALUES(?)";
			
			//PreparedStatement used to Prevent SQL injection attacks.
			PreparedStatement preStem = conn.prepareStatement(query);
			/*
			 * convertSpecialChar method
			 * The convertSpecialChar method used to check if the user input contains any HTML tags, it will remove the tags and put the value as it is.
			 *  I could use another method, but I used an easy way because security is not required
			 * */
			preStem.setString(1, convertSpecialChar(category.getCategoryName() )); 
			status = preStem.executeUpdate();
		} catch (Exception e) {
			throw new IllegalArgumentException("The category name should be unique, you can't have two categories with the same name "); 
		}
		return status;
	}
	
	/*
	 * @param (Category)
	 * The method update is responsible for updating an existing category.
	 *  If the user input invalid id for some reasons or input invalid data it will show an error message
	 *  to the end-user otherwise it will be updating the record in the database. 
	 * */
	//Update an existing record
	public static int update(Category category) throws IllegalAccessException {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement preStem = conn.prepareStatement("UPDATE Category SET CategoryName=? WHERE Id=?");
			preStem.setString(1, convertSpecialChar(category.getCategoryName())); 
			preStem.setInt(2, category.getId()); 
			status = preStem.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalAccessException("An issue occurred while updating, please check your input data"); 
		}
		return status;
	}
	
	/*
	 * @param (int)
	 * The method delete is responsible for deleting an existing category.
	 * If the category has no library items that refer to it, it will be deleted, otherwise
	 * it will show an error message to the end-user.
	 * */	
	//Delete a record
		public static int delete(int id) {
			int status= 0;
			try {
				Connection conn = DBConnection.getConnection();
				PreparedStatement preStem = conn.prepareStatement("DELETE FROM Category WHERE Id=?");
				preStem.setInt(1, id); 
				status = preStem.executeUpdate();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw new IllegalArgumentException("You can't delete a category that still has library items that refer to it."); 
			}
			return status;
		}	

		/*
		 * @param ()
		 * The method getAllRecords is responsible for returning a list of all categories in the database.
		 * If something went wrong, it will show an error message to the end-user.
		 * */		
		//Get All records 
		public static List<Category> getAllRecords() throws IllegalAccessException {
			//list: is a list of the Category Object, that will contains all records from the database.
			List<Category> list = new ArrayList<Category>();
			try {
				Connection conn = DBConnection.getConnection();
				PreparedStatement preStem = conn.prepareStatement("SELECT * FROM Category");
				//Get the result
				ResultSet response = preStem.executeQuery();
				while (response.next()) {
					//Every time initializes a new category Object and sets its value, then add it to the list.
					Category category = new Category();
					category.setId(response.getInt("Id"));
					category.setCategoryName(response.getString("CategoryName"));
					list.add(category);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw new IllegalAccessException("An issue occurred while fetching data from the database, please check your connection!"); 
			}
			return list;
		}		

		/*
		 * @param (id)
		 * The method getOneRecord is responsible for returning a Category Object from the database.
		 * If something went wrong, or the id is invalid it will show an error message to the end-user.
		 * */		
		//Get One record
		public static Category getOneRecord(int id) throws IllegalAccessException {
			//Create a Category Object and initializes it to null.
			Category category = null;
			try {
				Connection conn = DBConnection.getConnection();
				PreparedStatement preStem = conn.prepareStatement("SELECT * FROM Category WHERE Id=?");
				preStem.setInt(1,id);
				//Get the result
				ResultSet response = preStem.executeQuery();
				while (response.next()) {
					//If there is a response that comes from the database, will initializes a new libraryItem Category and sets its value, to return it.
					category = new Category();
					category.setId(response.getInt("Id"));
					category.setCategoryName(response.getString("CategoryName"));
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw new IllegalAccessException("An issue occurred while fetching data from the database, please check your connection and the input Id!"); 
			}
			return category;
		}
		
		/*
		 * convertSpecialChar method
		 * @param(String)
		 * The convertSpecialChar method used to check if the user input contains any HTML tags, it will remove the tags and put the value as it is.
		 *  I could use another method, but I used an easy way because security is not required
		 * */
		private static String convertSpecialChar(String htmlString) {	
		 	htmlString = htmlString.replaceAll("\\<.*?\\>", "");
			htmlString = htmlString.replaceAll("\r", "<br/>");
			htmlString = htmlString.replaceAll("\n", "<br/>");
			htmlString = htmlString.replaceAll("\"","&quot;");
			htmlString = htmlString.replaceAll("\'","&#39;");
			htmlString = htmlString.replaceAll(",","&#44;");
		 	return htmlString;
	}
}
