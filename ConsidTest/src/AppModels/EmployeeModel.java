package AppModels;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import AppObjects.Employee;
import Config.DBConnection;

/*
 * The class EmployeeModel is a Model that will deal with the Object Employee to avoid having the Object 
 * behaviors and the Database Connection in one Class, and to separate the classes according to their behaviors. 
 * */
public class EmployeeModel {

	/*
	 * @param (Employee)
	 * The method insert is responsible for adding a new employee to the database.
	 * If the employee was assigned as a CEO, we will check if there is a CEO in the Database
	 * or not because the system can have only one CEO. If there is no CEO the record will be added 
	 * otherwise it will show an error message to the end-user.
	 * If the employee was assigned as a Manager or Regular Employee, the record will be added to the database.
	 * */
	//Add new record
	public static int insert(Employee employee) throws IllegalAccessException {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "";
			PreparedStatement preStem = null;
			//Check if the user adding new CEO
			if(employee.isIsCEO()) {
				/*isCEOExist() is a boolean Method that will check if there is an existing CEO in the database or not.
				 * If it returned true, the system will show an error message to the end-user the it's not possible to have two CEO.
				 * If it returned false, which mean there is not CEO in the database, the record will be added to the database.
				 * convertSpecialChar() is a method that checks if the user input contains  any HTML tags. (See The method description)
				 * */
				if(isCEOExist()) {
					throw new IllegalAccessException("You can't have more than one CEO!"); 
				}else {
					//Add CEO!
					query = "INSERT INTO Employees (FirstName, LastName, Salary, IsCEO,IsManager) VALUES(?,?,?,?,?)";
					preStem = conn.prepareStatement(query);
					preStem.setString(1, convertSpecialChar(employee.getFirstName())); 
					preStem.setString(2, convertSpecialChar(employee.getLastName())); 
					preStem.setDouble(3, calculateSalary(employee));
					preStem.setBoolean(4, employee.isIsCEO());
					//No one can manage the CEO, that why the IsManager is set false and there is no data for ManagerId here.
					preStem.setBoolean(5, false);
					status = preStem.executeUpdate();
				}
			//If the user adding new Manager or Employee
			}else {
				//To Add Manager or Employee
				query = "INSERT INTO Employees (FirstName, LastName, Salary, IsCEO, IsManager, ManagerId) VALUES(?,?,?,?,?,?)";
				preStem = conn.prepareStatement(query);
				preStem.setString(1, convertSpecialChar(employee.getFirstName())); 
				preStem.setString(2, convertSpecialChar(employee.getLastName()));
				preStem.setDouble(3, calculateSalary(employee));
				preStem.setBoolean(4, false);
				preStem.setBoolean(5, employee.isIsManager());
				//Before adding the employee check if the ManagerId exist or not.
				preStem.setObject(6, employee.getManagerId() > 0 ? employee.getManagerId() : null);
				status = preStem.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalAccessException("You can't have more than one CEO!"); 
		}
		return status;
	}
	
	/*
	 * @param (Employee)
	 * The method update is responsible for updating an existing employee.
	 *  If the user input invalid id for some reasons or input invalid data it will show an error message
	 *  to the end-user otherwise it will be updating the record in the database. 
	 *  NOTE: There is an assumption that the end-user will NOT downgrade the employee position.
	 *  e.g., update the CEO to be Manager/Employee or update a Manager to be Employee
	 * */
	//Update an existing record
	public static int update(Employee employee) {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "UPDATE Employees SET FirstName =?, LastName=?, Salary=?, IsCEO=?, IsManager=?, ManagerId=? WHERE Id=?";
			PreparedStatement preStem = conn.prepareStatement(query);
			preStem.setString(1, convertSpecialChar(employee.getFirstName())); 
			preStem.setString(2, convertSpecialChar(employee.getLastName()));
			preStem.setDouble(3, calculateSalary(employee));
			preStem.setBoolean(4, employee.isIsCEO());
			preStem.setBoolean(5, employee.isIsManager());
			//Before updating the employee check if the ManagerId exist or not.
			preStem.setObject(6, employee.getManagerId() > 0 ? employee.getManagerId() : null);
			preStem.setInt(7, employee.getId());
			status = preStem.executeUpdate();
			if(status ==0) {
				throw new IllegalArgumentException("An issue occurred while updating, please check your input data");
			}
		} catch (Exception e) {
			 System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while updating, please check your input data"); 
		}
		return status;
	}
		
	/*
	 * @param (int)
	 * The method delete is responsible for deleting an existing employee.
	 * If the employee was a Manager or CEO it will check first if that Manager/ CEO still manage other employees, because it's
	 * not possible to delete Manager/ CEO that still manage other employees.
	 * If the Manager/ CEO still manage other employees it will show an error message to the end-user, 
	 * otherwise the employee will be deleted.
	 * */	
	//Delete a record
	public static int delete(int id) {
		int status= 0;
		//Check if the selected employee manage others or not
		/*
		 * isManageOthers(id) is a boolean method that will check if id is for Manager/CEO that still manage other employees.
		 * If it returned true, that means you can't delete the Manager/CEO.
		 * If it returned false, the system will delete the record.
		 * */
		if(!isManageOthers(id)) {
			try {
				Connection conn = DBConnection.getConnection();
				String query = "DELETE FROM Employees WHERE Id=?";
				PreparedStatement preStem = conn.prepareStatement(query);
				preStem.setInt(1, id); 
				status = preStem.executeUpdate();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw new IllegalArgumentException("An issue occurred while deleting, please check your input data"); 
			}
		}else {
			throw new IllegalArgumentException("You can't delete a Manager/CEO that still manage other employees!"); 
		}
		
		return status;
	}	

	/*
	 * @param ()
	 * The method getAllRecords is responsible for returning a list of all employees in the database.
	 * If something went wrong, it will show an error message to the end-user.
	 * This method will be use in the Employee View 2 that will show the user the manager ids instead of
	 * manager names, see the getAllRecordsWithManagerNames() method.
	 * NOTE: since I'm using the DataTable in the fornt-end, it's possible to remove the "ORDER BY IsCEO DESC, IsManager DESC" part
	 * from the query because I specified that the table should be sorted by Role when opening the Employee page.
	 * */		
	//Get All records  
	public static List<Employee> getAllRecords() {
		//list: is a list of the Employee Object, that will contains all records from the database.
		List<Employee> list = new ArrayList<Employee>();
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT * FROM Employees ORDER BY IsCEO DESC, IsManager DESC";
			PreparedStatement preStem = conn.prepareStatement(query);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				//Every time initializes a new employee Object and sets its value, then add it to the list.
				Employee employee = new Employee();
				employee.setId(response.getInt("Id"));
				employee.setFirstName(response.getString("FirstName"));
				employee.setLastName(response.getString("LastName"));
				employee.setSalary(response.getDouble("Salary"));
				employee.setIsCEO(response.getBoolean("IsCEO"));
				employee.setIsManager(response.getBoolean("IsManager"));
				employee.setManagerId(response.getInt("ManagerId"));
				/*
				 * The method getRoleValue() used to save the Role as String (Employee, Manager or CEO) in addition to the boolean.
				 * */
				employee.setRole(getRoleValue(employee));
				list.add(employee);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection!");
		}
		return list;
	}
	
	/*
	 * @param ()
	 * The method getAllRecordsWithManagerNames is responsible for returning a list of all employees in the database.
	 * It has the same behavior as the getAllRecords() described above, BUT this method will be use in the Employee View 1.
	 * It's a design decision, that will show the user the Manager name instead of the manager id.
	 * If something went wrong, it will show an error message to the end-user.
	 * NOTE: since I'm using the DataTable in the fornt-end, it's possible to remove the "ORDER BY IsCEO DESC, IsManager DESC" part
	 * from the query because I specified that the table should be sorted by Role when opening the Employee page.
	 * */		
	//Get All records  
	public static List<Employee> getAllRecordsWithManagerNames() {
		List<Employee> list = new ArrayList<Employee>();
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT *, "
					+ "(SELECT FirstName FROM Employees as FNCoulmn WHERE FNCoulmn.Id = employee.ManagerId) as managerFN"
					+ " ,(SELECT LastName FROM Employees as LNCoulmn WHERE LNCoulmn.Id = employee.ManagerId) as managerLN"
					+ " From Employees as employee ORDER BY IsCEO DESC, IsManager DESC";
			PreparedStatement preStem = conn.prepareStatement(query);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				//Every time initializes a new employee Object and sets its value, then add it to the list.
				Employee employee = new Employee();
				employee.setId(response.getInt("Id"));
				employee.setFirstName(response.getString("FirstName"));
				employee.setLastName(response.getString("LastName"));
				employee.setSalary(response.getDouble("Salary"));
				employee.setIsCEO(response.getBoolean("IsCEO"));
				employee.setIsManager(response.getBoolean("IsManager"));
				employee.setManagerId(response.getInt("ManagerId"));
				/*
				 * The method getRoleValue() used to save the Role as String (Employee, Manager or CEO) in addition to the boolean.
				 * */
				employee.setRole(getRoleValue(employee));
				employee.setManagerFullName(response.getString("managerFN") +" "+ response.getString("managerLN"));
				list.add(employee);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection!");
		}
		return list;
	}

	/*
	 * @param (id)
	 * The method getOneRecord is responsible for returning an Employee Object from the database.
	 * If something went wrong, or the id is invalid it will show an error message to the end-user.
	 * */		
	//Get One record	
	public static Employee getOneRecord(int id) {
		//Create an Employee Object and initializes it to null.
		Employee employee = null;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT * FROM Employees WHERE Id=?";
			PreparedStatement preStem = conn.prepareStatement(query);
			preStem.setInt(1,id);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				//If there is a response that comes from the database, will initializes a new Employee Object and sets its value, to return it.
				employee = new Employee();
				employee.setId(response.getInt("Id"));
				employee.setFirstName(response.getString("FirstName"));
				employee.setLastName(response.getString("LastName"));
				employee.setSalary(response.getDouble("Salary"));
				employee.setIsCEO(response.getBoolean("IsCEO"));
				employee.setIsManager(response.getBoolean("IsManager"));
				employee.setManagerId(response.getInt("ManagerId"));
				/*
				 * The method getRankBySalary() used to show the end-user the employee rank to update it if he/she wants
				 * */
				employee.setRank(getRankBySalary(employee));
				}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection");
		}
		return employee;
	}
	
	
	
	/*
	 * //For Edit!
	 * @param (int,boolean,boolean)
	 * The method getPossiableManagers is responsible for returning a list of all possible manager(s) for an employee 
	 * when the user trying to edit an employee/manager.
	 * The list will be used in the edit employee form, in the <select></select> tag.
	 * */		
	//Get possible managers records  
	public static List<Employee> getPossiableManagers(int id, boolean isCEO){
		//Every time initializes a new employee Object and sets its value, then add it to the list.
		List<Employee> list = new ArrayList<Employee>();
		Employee employee = null;
		try {
			Connection conn = DBConnection.getConnection();
			String query="";
			PreparedStatement preStem =null;
			//If the employee not CEO, then can be either Regular Employee or Manager (that not have any manger manage him/her)
			if(!isCEO) {
				query="SELECT * FROM Employees WHERE  IsManager = ? and Id!= ?";
				preStem = conn.prepareStatement(query);
				//The isManager is true because we want to return all managers except the selected id if it refers to a manager.
				preStem.setBoolean(1, true);
				preStem.setInt(2, id);
				ResultSet response = preStem.executeQuery();
				while (response.next()) {
					employee = new Employee();
					employee.setId(response.getInt("Id"));
					employee.setFirstName(response.getString("FirstName"));
					employee.setLastName(response.getString("LastName"));
					employee.setIsCEO(response.getBoolean("IsCEO"));
					employee.setIsManager(response.getBoolean("IsManager"));
					/*
					 * The method getRoleValue() used to save the Role as String (Employee, Manager or CEO) in addition to the boolean.
					 * */
					employee.setRole(getRoleValue(employee));
					list.add(employee);
					}
			}else {
				//If the employee is Manager or CEO as well
				query="SELECT * FROM Employees WHERE (IsCEO =? OR IsManager = ?) and Id != ?";
				preStem = conn.prepareStatement(query);
				//The isCEO and isManager are true because we want to return all managers and the CEO except the selected id if it refers to a manager.
				preStem.setBoolean(1, true);
				preStem.setBoolean(2, true);
				preStem.setInt(3, id);
				ResultSet response = preStem.executeQuery();
				while (response.next()) {
					employee = new Employee();
					employee.setId(response.getInt("Id"));
					employee.setFirstName(response.getString("FirstName"));
					employee.setLastName(response.getString("LastName"));
					employee.setIsCEO(response.getBoolean("IsCEO"));
					employee.setIsManager(response.getBoolean("IsManager"));
					employee.setRole(getRoleValue(employee));
					list.add(employee);
					}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection!");
		}
		return list;
	}
	
	
	/*
	 * //For Add!
	 * @param (int,boolean,boolean)
	 * The method getAllManagers is responsible for returning a list of all manager(s) including the CEO to show it 
	 * to the end-user when the user trying to adding an new employee ( employee/manager/CEO).
	 * The list will be used in the edit employee form, in the <select></select> tag.
	 * */		
	//Get ALL managers records  
	public static List<Employee> getAllManagers(){
		List<Employee> list = new ArrayList<Employee>();

		try {
			Connection conn = DBConnection.getConnection();
			String query="SELECT * FROM Employees WHERE (IsCEO = true OR IsManager = true)";
			PreparedStatement preStem = conn.prepareStatement(query);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				Employee resultEmployees = new Employee();
				resultEmployees.setId(response.getInt("Id"));
				resultEmployees.setFirstName(response.getString("FirstName"));
				resultEmployees.setLastName(response.getString("LastName"));			 
				resultEmployees.setIsCEO(response.getBoolean("IsCEO"));
				resultEmployees.setIsManager(response.getBoolean("IsManager")); 
				/*
				 * The method getRoleValue() used to save the Role as String (Employee, Manager or CEO) in addition to the boolean.
				 * */
				resultEmployees.setRole(getRoleValue(resultEmployees));
				list.add(resultEmployees);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection!");
		}
		return list;
	}
	

	/*
	 * Design decision!
	 * @param(Employee)
	 * The method getRoleValue() used to save the Role as String (Employee, Manager or CEO) in addition to the boolean.
	 * It will take an employee and check
	 * if the employee isCEO true will return "CEO",
	 * if the employee isCEO false and isManager is true will return "Manager"
	 * if the employee isCEO false and isManager is false will return "Employee"
	 * 
	 * */
	private static String getRoleValue(Employee employee) {
		String result ="";
		if(employee.isIsCEO()) {
			result = "CEO";
		}else if(employee.isIsManager()) {
			result = "Manager";
		}else {
			result = "Employee";
		}
		return result;
	}
	
	/*
	 * @param()
	 * isCEOExist() is a boolean Method that will check if there is an existing CEO in the database or not.
	 * If it returned true, the system will show an error message to the end-user the it's not possible to have two CEO.
	 * If it returned false, which mean there is not CEO in the database, the record will be added to the database.
	 * */
	private static boolean isCEOExist() throws IllegalAccessException {
		try {
			Connection conn = DBConnection.getConnection();
			String query = "";
			PreparedStatement preStem = null;
			//Check if there is any CEO in the DB
			query = "SELECT IsCEO FROM Employees WHERE IsCEO = true;";
			preStem = conn.prepareStatement(query);
			ResultSet response = preStem.executeQuery();
			if(response.next()) {
				throw new IllegalAccessException("You can't have more than one CEO!"); 
			}else {
				return false;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalAccessException("You can't have more than one CEO!"); 
		}
	}
	
	/*
	 * //For Delete!
	 * @param(id)
	 * The method isManageOthers() will check if the given id is for a manager that still manager other employees/manager or not.
	 * If it returned true,that means the given id is for an employee that not managing anyone else.
	 * If it returned false, that mean there is some employees/manager that are managed by the given manager id.
	 * */
	private static boolean isManageOthers(int id) {
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT GROUP_CONCAT(id) as ids FROM Employees where ManagerId = ?";
			PreparedStatement preStem = conn.prepareStatement(query);	
			preStem.setInt(1, id);
			ResultSet response = preStem.executeQuery();
			if(response.next()) {
				if(response.getObject("ids") ==null) {
					return false;
				}else {
					return true;
				}
				
			}else {
				throw new IllegalAccessException("You can't delete a Manager/CEO that still manage other employees!"); 
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("You can't delete a Manager/CEO that still manage other employees!"); 
		}
	}

	/*
	 * @param(Employee)
	 * The method calculateSalary() used to calculate the employee Salary based on his/her role.
	 * The variables: employeeCoefficient, managerCoefficient and CEOCoefficient used to avoid magic numbers
	 * Magic number: is a number that user somewhere without declaring what that name is 
	 * Magic number: can be changed easily.
	 * The returned value will be rounded up to have only 2 decimal numbers after the comma.
	 * 
	 * */
	private static double calculateSalary(Employee employee) {
		double employeeCoefficient = 1.125;
		double managerCoefficient = 1.725;
		double CEOCoefficient = 2.725;
		double hundred = 100.0;
		double result = 0;
		if(employee.isIsCEO()) {
			result = employee.getRank() * CEOCoefficient;
		}else if(employee.isIsManager()) {
			result = employee.getRank() *managerCoefficient ;
		}else {
			result = employee.getRank() *employeeCoefficient ;
		}

		//To round the result up I multiply by hundred and divided by hundred to have only two decimal numbers after the comma.
		return Math.round(result * hundred) / hundred;
	}
	
	/*
	 * @param(Employee)
	 * The method getRankBySalary() used to show the end-user the employee rank to update it if he/she wants
	 * Same as the previous method calculateSalary(), it use the variables: employeeCoefficient, managerCoefficient and CEOCoefficient.
	 * */
	private static int getRankBySalary(Employee employee) {
		double employeeCoefficient = 1.125;
		double managerCoefficient = 1.725;
		double CEOCoefficient = 2.725;
		int result = 0;
		if(employee.isIsCEO()) {
			result = (int) ( employee.getSalary() / CEOCoefficient);
		}else if(employee.isIsManager()) {
			result = (int) (employee.getSalary()  / managerCoefficient) ;
		}else {
			result = (int) (employee.getSalary()  / employeeCoefficient) ;
		}
		return result;
	}
	
	/*
	 * convertSpecialChar method
	 * @param(String)
	 * The convertSpecialChar method used to check if the user input contains any HTML tags, it will remove the tags and put the value as it is.
	 *  I could use another method, but I used an easy way because security is not required
	 * */
	private static String convertSpecialChar(String str) {	
	 	str = str.replaceAll("\\<.*?\\>", "");
		str = str.replaceAll("\r", "<br/>");
		str = str.replaceAll("\n", "<br/>");
		str = str.replaceAll("\"","&quot;");
		str = str.replaceAll("\'","&#39;");
		str = str.replaceAll(",","&#44;");
	 	return str;
	}
	
}
