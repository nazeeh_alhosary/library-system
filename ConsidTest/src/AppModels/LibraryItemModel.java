package AppModels;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import AppObjects.LibraryItem;
import Config.DBConnection;


/*
 * The class LibraryItemModel is a Model that will deal with the Object LibraryModel to avoid having the Object 
 * behaviors and the Database Connection in one Class, and to separate the classes according to their behaviors. 
 * */
public class LibraryItemModel {
	
	/*
	 * @param (LibraryItem)
	 * The method insert is responsible for adding a new libraryItem to the database.
	 * It will check the type of that libraryItem then insert the required fields for each type.
	 * */
	//Add new record
	public static int insert(LibraryItem libraryItem) throws IllegalAccessException {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "";
			PreparedStatement preStem = null;
			//Check the type and insert the required fields for each type.
			switch(libraryItem.getType()) {
			  case "Book":
			  case "Reference Book":
				  query = "INSERT INTO LibraryItem (CategoryId,Title,Author,Pages,IsBorrowable,Borrower,BorrowDate,Type) VALUES(?,?,?,?,?,?,?,?)";
				  preStem = conn.prepareStatement(query);
				  preStem.setString(3, convertSpecialChar(libraryItem.getAuthor())); 
				  preStem.setInt(4, libraryItem.getPages()); 
				  break;
			  case "DVD":
			  case "Audio Book":
				 query = "INSERT INTO LibraryItem (CategoryId,Title,Author,RunTimeMinutes,IsBorrowable,Borrower,BorrowDate,Type) VALUES(?,?,?,?,?,?,?,?)";
				 preStem = conn.prepareStatement(query); 
				 preStem.setString(3, ""); 
				 preStem.setInt(4, libraryItem.getRunTimeMinutes()); 				  
			    break;
			} 
			
			//After checking the type, insert the required fields for all types.
			  preStem.setInt(1, libraryItem.getCategoryId()); 
			  /*
			   * The getAcronymTitle method will return the title with Acronym in parentheses 
			   * Also it will use the convertSpecialChar() method to not adding any HTML tags in the database
			   * */
			  preStem.setString(2,  getAcronymTitle(libraryItem.getTitle()));
			  preStem.setBoolean(5, libraryItem.isIsBorrowable());
			
			  //Check the type is Reference Book because the Reference Book can not be borrowed home and the fields IsBorrowable & Borrower should be set to null
			  if(!libraryItem.isIsBorrowable() && !libraryItem.getType().equalsIgnoreCase("Reference Book")) {
				 String currentDate = java.time.LocalDate.now().toString(); 
				  preStem.setString(6, convertSpecialChar(libraryItem.getBorrower()));
				  preStem.setString(7, currentDate); 
			  }else {
				  preStem.setString(6, "");
				  preStem.setString(7, null);
			  }
			  preStem.setString(8, libraryItem.getType()); 
			  status = preStem.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalAccessException("An issue occurred while adding, please check your input data"); 
		}
		return status;
	}

	/*
	 * @param (LibraryItem)
	 * The method update is responsible for updating an existing libraryItem.
	 *  If the user input invalid id for some reasons or input invalid data it will show an error message
	 *  to the end-user otherwise it will be updating the record in the database. 
	 *  The method will check the Type first and then set the required fields as the insert() method
	 * */
	//Update an existing record	
		public static int update(LibraryItem libraryItem) {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "";
			PreparedStatement preStem = null;
			switch(libraryItem.getType()) {
			  case "Book":
			  case "Reference Book":
				  query = "UPDATE LibraryItem SET CategoryId=?, Title=?, Author=?, Pages=?, RunTimeMinutes=NULL, Type=? WHERE Id=?";
				  preStem = conn.prepareStatement(query);
				  preStem.setString(3, convertSpecialChar(libraryItem.getAuthor())); 
				  preStem.setInt(4, libraryItem.getPages()); 
				  
				  break;
			  case "DVD":
			  case "Audio Book":
				 query = "UPDATE LibraryItem SET CategoryId=?, Title=?, Author=?, Pages =NULL ,RunTimeMinutes=? , Type=? WHERE Id=?";
				 preStem = conn.prepareStatement(query); 
				 preStem.setString(3, ""); 
				 preStem.setInt(4, libraryItem.getRunTimeMinutes()); 				  
			    break;
			}				
			preStem.setInt(1, libraryItem.getCategoryId());
			preStem.setString(2, convertSpecialChar(libraryItem.getTitle())); 
			preStem.setString(5, libraryItem.getType()); 
			preStem.setInt(6, libraryItem.getId()); 
			status = preStem.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while updating, please check your input data"); 
		}
		return status;
	}

		
	/*
	 * @param (int)
	 * The method delete is responsible for deleting an existing employee.
	 * If the user input invalid id for some reasons or input invalid data it will show an error message.
	 * */	
	//Delete a record
	public static int delete(int id) {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "DELETE FROM LibraryItem WHERE Id=?";
			PreparedStatement preStem = conn.prepareStatement(query);
			preStem.setInt(1, id); 
			status = preStem.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while deleting, please check your input data"); 
		}
		return status;
	}	
		
	/*
	 * @param ()
	 * The method getAllRecords is responsible for returning a list of all library items in the database.
	 * If something went wrong, it will show an error message to the end-user.
	 * NOTE: There is a JOIN between the Employee table and the Category table to fetch the Category name instead of displaying the category id.
	 * */		
	//Get All records  
	public static List<LibraryItem> getAllRecords() {
		//list: is a list of the LibraryItem Object, that will contains all records from the database.
		List<LibraryItem> list = new ArrayList<LibraryItem>();
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT LibraryItem.*, Category.CategoryName FROM LibraryItem LEFT JOIN Category ON Category.Id = LibraryItem.CategoryId";
			PreparedStatement preStem = conn.prepareStatement(query);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				//Every time initializes a new libraryItem Object and sets its value, then add it to the list.
				LibraryItem libraryItem = new LibraryItem();
				libraryItem.setId(response.getInt("Id"));
				libraryItem.setCategoryId(response.getInt("CategoryId"));
				libraryItem.setTitle(response.getString("Title"));
				libraryItem.setAuthor(response.getString("Author"));
				libraryItem.setPages(response.getInt("Pages"));
				libraryItem.setRunTimeMinutes(response.getInt("RunTimeMinutes"));
				libraryItem.setIsBorrowable(response.getBoolean("IsBorrowable"));
				libraryItem.setBorrower(response.getString("Borrower"));
				libraryItem.setBorrowDate(response.getString("BorrowDate"));
				libraryItem.setType(response.getString("Type"));
				libraryItem.setCategoryName(response.getString("CategoryName"));
				list.add(libraryItem);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection");
		}
		return list;
	}	
	
	/*
	 * @param (int)
	 * The method getOneRecord is responsible for returning a LibraryItem Object from the database.
	 * If something went wrong, or the id is invalid it will show an error message to the end-user.
	 * */		
	//Get One record
	public static LibraryItem getOneRecord(int id) {
		//Create a LibraryItem Object and initializes it to null.
		LibraryItem libraryItem = null;
		try {
			Connection conn = DBConnection.getConnection();
			String query = "SELECT LibraryItem.*, Category.CategoryName FROM LibraryItem LEFT JOIN Category ON Category.Id = LibraryItem.CategoryId WHERE LibraryItem.Id=?";
			PreparedStatement preStem = conn.prepareStatement(query);
			preStem.setInt(1,id);
			//Get the result
			ResultSet response = preStem.executeQuery();
			while (response.next()) {
				//If there is a response that comes from the database, will initializes a new libraryItem Object and sets its value, to return it.
				libraryItem = new LibraryItem();
				libraryItem.setId(response.getInt("Id"));
				libraryItem.setCategoryId(response.getInt("CategoryId"));
				libraryItem.setTitle(response.getString("Title"));
				libraryItem.setAuthor(response.getString("Author"));
				libraryItem.setPages(response.getInt("Pages"));
				libraryItem.setRunTimeMinutes(response.getInt("RunTimeMinutes"));
				libraryItem.setIsBorrowable(response.getBoolean("IsBorrowable"));
				libraryItem.setBorrower(response.getString("Borrower"));
				libraryItem.setBorrowDate(response.getString("BorrowDate"));
				libraryItem.setType(response.getString("Type"));
				libraryItem.setCategoryName(response.getString("CategoryName"));
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage() );
			throw new IllegalArgumentException("An issue occurred while fetching data from the database, please check your connection");
		}
		return libraryItem;
	}

	/*
	 * @param (int)
	 * The method checkIn is responsible for updating a LibraryItem when the user checkIn a libraryItem.
	 * If something went wrong, or the id is invalid it will show an error message to the end-user.
	 * */		
	//Check in a Library item
	public static int checkIn(int id) {
		int status= 0;
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement preStem = conn.prepareStatement("UPDATE LibraryItem SET IsBorrowable=?, Borrower=?, BorrowDate=? WHERE Id=?");
			preStem.setBoolean(1, true);
			preStem.setString(2, ""); 
			preStem.setString(3, null);
			preStem.setInt(4, id);  
			status = preStem.executeUpdate();
			if(status ==0) {
				throw new IllegalArgumentException("The id: " + id + " is not valid!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while checking in an item , please check your input data");
		}
		return status;
	}
	
	/*
	 * @param (int)
	 * The method checkOut is responsible for updating a LibraryItem when the user checkOut a libraryItem.
	 * The checkOut will use the current data and set it to the BorrowDate.
	 * If something went wrong, or the id is invalid it will show an error message to the end-user.
	 * */		
	//Check in a Library item
	public static int checkOut(LibraryItem libraryItem) {
		int status= 0;
		String currentDate = java.time.LocalDate.now().toString(); 
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement preStem = conn.prepareStatement("UPDATE LibraryItem SET IsBorrowable=?, Borrower=?, BorrowDate=? WHERE Id=?");
			preStem.setBoolean(1, false);
			preStem.setString(2, libraryItem.getBorrower()); 
			preStem.setString(3, currentDate);
			preStem.setInt(4, libraryItem.getId());  
			status = preStem.executeUpdate();
			if(status ==0) {
				throw new IllegalArgumentException("The id: " + libraryItem.getId() + " is not valid!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new IllegalArgumentException("An issue occurred while checking out an item , please check your input data");
		}
		return status;
	}
	
	
	/**
	 * @param(String)
	 * The getAcronymTitle method will return the title with Acronym in parentheses.
	 * It will go through all the input string and check if there is a space which means it's a new word. 
	 * If will split the input string every time we find a space (after convert it to UpperCase) and add the first character to the return variable.
	 * Also it will use the convertSpecialChar() method to not adding any HTML tags in the database
	 * */
	private static String getAcronymTitle (String str) {
		str = convertSpecialChar(str);
		String acronym  = "(";
		for (String sub_str : str.toUpperCase().split(" ")) {
			acronym +=sub_str .charAt(0);
		}
		acronym +=")";
		return str + " " +acronym ;
	}
	
	/*
	 * convertSpecialChar method
	 * @param(String)
	 * The convertSpecialChar method used to check if the user input contains any HTML tags, it will remove the tags and put the value as it is.
	 *  I could use another method, but I used an easy way because security is not required
	 * */
	private static String convertSpecialChar(String htmlString) {	
	 	htmlString = htmlString.replaceAll("\\<.*?\\>", "");
		htmlString = htmlString.replaceAll("\r", "<br/>");
		htmlString = htmlString.replaceAll("\n", "<br/>");
		htmlString = htmlString.replaceAll("\"","&quot;");
		htmlString = htmlString.replaceAll("\'","&#39;");
		htmlString = htmlString.replaceAll(",","&#44;");
	 	return htmlString;
	}
	

}
