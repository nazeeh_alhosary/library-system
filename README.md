# Library system - Consid 
The library system is a web application powered by dynamic Java web application utilities. 
  - Manage library items such as Book, DVD, Audio Book and Reference Book
  - Borrow and return items
  - Manage employees 
#  Features
  - Modifiable dynamic offline library where data can be added, removed and edited 
  - Information regarding each item are available such as the title, category, author and date
  - Item Availability can be viewed
  - Actions such as checking in or chcking out an item can be performed
  - A user can sort and search for an item depedning on any of the availble parameters
  - An item category can be added, removed or edited 
  - A user can sort and search for a category depedning on the name
  - As a user, you can add or remove or edit employee information 
  - As a user, you can calculate the salary of employees based on a rank and a specified coefficient 
#  Assumptions & Design Decisions
* A log-in page exists, however, it's just used as a prove of concept  
* The title acronym is case-sensitive and will yield out an upper cases letter code.
* The Library system list will be, by default, sorted by category name and the user can resort the table as they wish. In addition, they can also sort by Type, Author, Title, etc...
    * The application restars when the user logs out from the applciation
* Since there was no description regarding the upgrade or downgrade of roles, a user can **upgrade** but not downgrade roles. 
    * For example, a CEO cannot become a mangager or employee. Meanwhile, an employee can become a manger or even a CEO considering all the specified requirements in the Acceptance Criterion.  
# Tech
The Library uses a number of open source projects to work properly:
* [Eclipse EE](https://www.eclipse.org/ide/)

* [Tomcat 9.0 or above](http://tomcat.apache.org/tomcat-9.0-doc/setup.html)

* [XAMPP](https://www.apachefriends.org/index.html)

* [Java JSP](https://en.wikipedia.org/wiki/JavaServer_Pages) 

* [Java Servlet](https://en.wikipedia.org/wiki/Java_servlet)

* [Bootstrap](https://getbootstrap.com/)

* [Sweet alert](http://sweetalert.js.org/)

* [Frontend Awesome](https://fontawesome.com/icons?d=gallery)

* [HTML](https://www.w3schools.com/html/) 

* [jQuery](https://jquery.com/)
# Configuration
##### Step 1
In your editor, go to the DBConnection file in the project through following the path specified below:
**Path**: javaResources/src/Config/DBConnection.java
##### Step 2
Input your credentials as specified below:
```sh
String dbName = "......"; //Your Database name
String dbUserName= "......"; //Your host user name (e.g., root)
String dbPassWord ="....."; // Your host password (e.g, "")
String dbHost = "....."; // Your HOST (localhost if loacl)
String dbPort = ".....";// Your Database PORT
```
*Note: Make sure the the database has the required colomns with the exact nomenclutre specified below:*
* Category
* Employees
* LibraryItem
##### Step 3
Run the Tomcat server from the IDE 
##### Step 4
Follow the outputted link by the Tomcat server
# Future Development 
 - Migrating the project and its services to become cloud-native
 - Enhance the frontend 
 - Focus more on UX 
 
[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>
   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
